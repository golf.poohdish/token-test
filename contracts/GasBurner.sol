 // SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Waste{
    address public owner;
    string public name;
    string public description;
    
    constructor(string memory _name, string memory _description) {
        owner = msg.sender;
        name = _name;
        description = _description;
    }

    function changeName(string memory _name) public {
        name = _name;
    }

    function changeDescription(string memory _description) public {
        description = _description;
    }
}


contract GasBurner {

    uint8 version = 3;

    constructor() {
    }

    event WasteAddress(address _address, address _by);

    function burnWithContract(uint8 _numberOfContracts) public {
        for (uint8 i = 0; i < _numberOfContracts; i++) {
            Waste waste = new Waste(
                string(abi.encodePacked("Waste Name ", i)), 
                string(abi.encodePacked("em Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", i))
            ); 
            emit WasteAddress(address(waste), msg.sender);
        }
    }

    function burnWithBareContract(uint8 _numberOfContracts) public {
        for (uint8 i = 0; i < _numberOfContracts; i++) {
            Waste waste = new Waste(
                "", 
                ""
            ); 
            emit WasteAddress(address(waste), msg.sender);
        }
    }

    function hogCPU(uint256 _numberOfContracts) public {
        for (uint256 i = 0; i < _numberOfContracts; i++) {
        }
    }
}
