// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Glimmer is ERC20 {
    constructor(uint256 initialSupply) ERC20("Glimmer", "GLM") {
        _mint(msg.sender, initialSupply);
    }
}
