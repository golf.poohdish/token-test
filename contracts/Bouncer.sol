 // SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/**
 * Bouncer purpose is to bounce internal transaction between two Bouncer contract.
 * Its intention is to be used for burning gas.
 *
 **/
contract Bouncer {

    uint8 version = 1;

    constructor() {
    }

    event BounceOut(address _sender, address _to, uint8 _round);
    event BounceEnd(address _sender, uint8 _round);
    
    function bounce(address _to, uint8 _round) public returns(uint8){
        if (_round <= 0) {
            emit BounceEnd(msg.sender, _round);
            return _round;
        }

        address[] memory addr = new address[](3);
        addr[0] = address(this);
        addr[1] = _to;
        addr[2] = msg.sender;
        wasteTime(addr, _round);
        
        Bouncer bouncer = Bouncer(_to);
        bouncer.bounce(address(this), _round - 1);
        emit BounceOut(msg.sender, _to, _round - 1);

        return _round - 1;
    }

    function wasteTime(address[] memory _addr, uint8 _round) private view {
        for(uint8 i = 0; i < _round; i++) {
            for(uint8 j = 0; j < _addr.length; j++) {
                address(_addr[j]).balance;
            }
        }
    }
}
